#!/usr/bin/env python3

import socket
import select
import sys

ENCODE = 'utf-8'


class TCPServer:
    def __init__(self, host_ip: str, port: int):
        self.host_ip = host_ip  # ip of server
        self.port = port  # port of server
        self.server = None  # object of socket
        self.sockets_list = []  # list of sockets (server, client1, client2...)
        self.readable = []  # all clients connected to server
        self.exceptional = []  # all failures

    # IPv4, TCP, non-blocking
    def set_server(self):
        self.server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.server.setblocking(False)
        self.server.bind((self.host_ip, self.port))
        self.server.listen(5)
        self.sockets_list = [self.server]

    # wait and establish new connections, return number of last connection
    def connect_clients(self) -> int:
        self.readable, _, self.exceptional = select.select(self.sockets_list, [], self.sockets_list)

        for s in self.readable:
            if s is self.server:
                connection, client_address = s.accept()
                connection.setblocking(False)
                self.sockets_list.append(connection)

        return len(self.sockets_list) - 1

    # receive all data sent by clients
    def receive_data(self):
        for s in self.readable:
            if s is not self.server:

                chunks = []
                read_data = True

                while read_data:
                    try:
                        decoded_data = str(s.recv(4096), ENCODE)
                    except:
                        decoded_data = ''
                        read_data = False

                    if decoded_data == '':
                        read_data = False

                    chunks.append(decoded_data)

                decoded_data = ''
                for x in chunks:
                    decoded_data = decoded_data + x

                if decoded_data:
                    # if client send "end" message then close connection with him
                    if decoded_data == 'end':
                        encoded_data = decoded_data.encode(ENCODE)
                        s.send(encoded_data)
                        self.sockets_list.remove(s)
                        s.close()

    # send message to all clients
    def send_all(self, msg: str):
        for s in self.sockets_list:
            if s is not self.server:
                encoded_data = msg.encode(ENCODE)
                s.send(encoded_data)

    # send msg to specific client
    def send_msg(self, client_number: int, msg: str):
        s = self.sockets_list[client_number]
        if s is not self.server:
            encoded_data = msg.encode(ENCODE)
            s.send(encoded_data)

    # Handle "exceptional conditions"
    def except_handle(self):
        for s in self.exceptional:
            self.sockets_list.remove(s)
            s.close()

    def get_sockets_list(self) -> []:
        return self.sockets_list


class TCPClient:
    def __init__(self, host_ip: str, port: int):
        self.host_ip: str = host_ip
        self.port: int = port
        self.client = None
        self.connection_established = 0

    # set connection with server
    def connect(self) -> bool:
        try:
            self.client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self.client.connect((self.host_ip, self.port))
            self.client.setblocking(False)
            self.connection_established = True
        except:
            print("Client couldn't establish connection with server", sys.stderr)
            self.connection_established = False

        return self.connection_established

    # send data to server
    def send_data(self, data_send):
        encoded_data = data_send.encode(ENCODE)
        self.client.sendall(encoded_data)

    # receive data from server
    def receive_data(self):
        chunks = []
        read_data = True

        while read_data:
            try:
                decoded_data = str(self.client.recv(4096), ENCODE)
            except:
                decoded_data = ''
                read_data = False

            if decoded_data == '':
                read_data = False

            chunks.append(decoded_data)

        decoded_data = ''
        for x in chunks:
            decoded_data = decoded_data + x

        return decoded_data

    #close connection with server
    def close_connection(self):
        self.client.close()
        self.connection_established = 0
