import TCPlib

ip = '192.168.93.22'
port = 65432
i = 1

# Choose mode
print("Write if you are server(1) or client(2): ")
c_s_mode = int(input("I am: "))

# --------------------server------------------------#

if c_s_mode == 1:
    TCP_S = TCPlib.TCPServer(ip, port)
    TCP_S.set_server()

    sockets = []

    while True:
        cn = TCP_S.connect_clients()
        sockets = TCP_S.get_sockets_list()
        if len(sockets) > 1:
            TCP_S.receive_data()

        # TCP_S.sockets_list[1->serwer,2->1 klient,3->2 klient]
        msg = 'wiadomosc od serwera'
        if len(sockets) > 1:
            print("ilosc polaczen: ", len(sockets)-1)
            #msg = str(input("nadaj wiadomosc: "))
            #cn = int(input("komu: "))
            #TCP_S.send_msg(cn, msg)
            TCP_S.except_handle()

        print("\npetla test")


# --------------------client------------------------#
elif c_s_mode == 2:

    # jak podam 'end' z to klient sie zapetla tutaj

    msg = ''
    TCP_C = TCPlib.TCPClient(ip, port)
    connected = TCP_C.connect()

    while connected:
        if msg != 'end':
            msg = str(input("nadaj wiadomosc: "))
            TCP_C.send_data(msg)
            print("Wyslano wiadomosc do serwera: ", msg)

        rcv_data = TCP_C.receive_data()
        print("Otrzymano z serwera: ", rcv_data)

        if rcv_data == 'end':
            connected = False

        print('ilosc petli w testach klient: ', i)

        i += 1
